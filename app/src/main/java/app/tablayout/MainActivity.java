package app.tablayout;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabItem;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    TabLayout tabLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);



        toolbar.setTitle(getResources().getString(R.string.app_name));
        tabLayout = findViewById(R.id.tablayout);
        TabItem tabChats = findViewById(R.id.tabChats);
        TabItem tabStatus = findViewById(R.id.tabStatus);
        TabItem tabCalls = findViewById(R.id.tabCalls);
        ViewPager viewPager = findViewById(R.id.viewPager);

        ArrayList<Fragment> listofFragments = new ArrayList<>();
        listofFragments.add(new ChatFragment());
        listofFragments.add(new StatusFragment());
        listofFragments.add(new CallFragment());


        int basket = getSharedPreferences("basket", Activity.MODE_PRIVATE).getInt("basketCount", 0);
        String basketTitle = "Basket (" + String.valueOf(basket) + ")";
        ArrayList<String> listofFragmentTitles = new ArrayList<>();

        listofFragmentTitles.add(basketTitle);
        listofFragmentTitles.add("Watching");
        listofFragmentTitles.add("Favortites");


        PageAdapter pageAdapter = new PageAdapter(getSupportFragmentManager(), listofFragments, listofFragmentTitles);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        viewPager.setOffscreenPageLimit(3);//KEEPS 3 TABS IN MEMORY
        viewPager.setAdapter(pageAdapter);
        tabLayout.setupWithViewPager(viewPager);//ON SETUP AND PAGE CHANGE, IT CHANGES THE TITLE




        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //GETTING INT FROM SHAREDPREF
                /*SharedPreferences sp = getSharedPreferences("basket", Activity.MODE_PRIVATE);
                int myIntValue = sp.getInt("basketCount", 0);*/
                // ON LINER
                //int myIntValue = getSharedPreferences("basket", Activity.MODE_PRIVATE).getInt("basketCount", 0);

                //PUTTING INT INTO SHAREDPREF
                /*SharedPreferences sp = getSharedPreferences("basket", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putInt("basketCount", 10);
                editor.commit();*/
                // ON LINER
                //getSharedPreferences("basket", Activity.MODE_PRIVATE).edit().putInt("basketCount", 10).commit();




                getSharedPreferences("basket", Activity.MODE_PRIVATE).edit().putInt("basketCount", (getSharedPreferences("basket", Activity.MODE_PRIVATE).getInt("basketCount", 0))+1).commit();



                int basket = getSharedPreferences("basket", Activity.MODE_PRIVATE).getInt("basketCount", 0);
                String basketTitle = "Basket (" + String.valueOf(basket) + ")";
                tabLayout.getTabAt(0).setText(basketTitle);

                Snackbar.make(view, "Item added to basket", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);





    }
}
