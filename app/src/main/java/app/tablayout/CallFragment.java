package app.tablayout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class CallFragment extends Fragment {
    public View callFragmenttView;


    public CallFragment(){}


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        callFragmenttView = inflater.inflate(R.layout.fragment_call, container, false);


        return callFragmenttView;

    }

    public static CallFragment newInstance(){
        CallFragment fragment = new CallFragment();
        return fragment;
    }

}
