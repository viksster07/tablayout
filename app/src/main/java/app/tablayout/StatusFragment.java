package app.tablayout;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class StatusFragment extends Fragment {
    public View statusFragmentView;


    public StatusFragment(){}


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        statusFragmentView = inflater.inflate(R.layout.fragment_status, container, false);


        return statusFragmentView;

    }

    public static StatusFragment newInstance(){
        StatusFragment fragment = new StatusFragment();
        return fragment;
    }

}

