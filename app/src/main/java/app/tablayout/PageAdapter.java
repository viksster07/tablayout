package app.tablayout;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;

public class PageAdapter extends FragmentPagerAdapter {

    private int numOfTabs;
    private ArrayList<Fragment> listofFragments;
    private ArrayList<String> listofFragmentTitles;

    PageAdapter(FragmentManager fm, ArrayList<Fragment> listofFragments, ArrayList<String> listofFragmentTitles) {
        super(fm);
        this.numOfTabs = listofFragmentTitles.size();
        this.listofFragments = listofFragments;
        this.listofFragmentTitles = listofFragmentTitles;
    }

    @Override
    public Fragment getItem(int position) {
        return listofFragments.get(position); //RETURNS FRAGMENT FROM LIST OF FRAGMENTS DEPENDING ON VALUE OF POSITION
/*
        switch (position) {
            case 0:
                return new ChatFragment();
            case 1:
                return listofFragments.get(1);
            case 2:
                return listofFragments.get(2);
            default:
                return null;
        }*/
    }

    @Override
    public int getCount() {
        return numOfTabs;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return listofFragmentTitles.get(position);
    }

}